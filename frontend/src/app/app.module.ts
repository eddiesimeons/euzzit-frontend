import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FormsModule } from '@angular/forms';
import { RegisterPage } from '../pages/register/register';
import { TabsPage } from '../pages/tabs/tabs';
import { AccountPage } from '../pages/account/account';
import { AirtimePage } from '../pages/airtime/airtime';
import { Register1Page } from '../pages/register1/register1';
import { VerifyOtpPage } from '../pages/verify-otp/verify-otp';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { AirtimeDataPage } from '../pages/airtime-data/airtime-data';
import { DataPage } from '../pages/data/data';
import { CabletvPage } from '../pages/cabletv/cabletv';
import { AirtimeConfirmationPage } from '../pages/airtime-confirmation/airtime-confirmation';
import { AirtimeTransactionPage } from '../pages/airtime-transaction/airtime-transaction';
import { AirtimeTransactionSucessfullPage } from '../pages/airtime-transaction-sucessfull/airtime-transaction-sucessfull';
import { CableConfirmationPage } from '../pages/cable-confirmation/cable-confirmation';
import { CableTransactionPage } from '../pages/cable-transaction/cable-transaction';
import { CreatePinPage } from '../pages/create-pin/create-pin';
import { DataConfirmationPage } from '../pages/data-confirmation/data-confirmation';
import { DataTransationPage } from '../pages/data-transation/data-transation';
import { ConfirmPinPage } from '../pages/confirm-pin/confirm-pin';
import { LoginWalletPage } from '../pages/login-wallet/login-wallet';
import { NetworkPopupPage } from '../pages/network-popup/network-popup';
import { CableNetworkPage } from '../pages/cable-network/cable-network';
import { CompanyPopupPage } from '../pages/company-popup/company-popup';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    TabsPage,
    AccountPage,
    AirtimePage,
    Register1Page,
    VerifyOtpPage,
    AirtimeDataPage,
    DataPage,
    CabletvPage,
    AirtimeConfirmationPage,
    AirtimeTransactionPage,
    AirtimeTransactionSucessfullPage,
    CableConfirmationPage,
    CableTransactionPage ,
    CreatePinPage,
    DataConfirmationPage,
    DataTransationPage,
    ConfirmPinPage,
    LoginWalletPage,
    NetworkPopupPage,
    CableNetworkPage,
    CompanyPopupPage,
  ],
  imports: [
    BrowserModule,FormsModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RegisterPage,
    TabsPage,
    AccountPage,
    AirtimePage,
    Register1Page,
    VerifyOtpPage,
    AirtimeDataPage,
    DataPage,
    CabletvPage,
    AirtimeConfirmationPage,
    AirtimeTransactionPage,
    AirtimeTransactionSucessfullPage,
    CableConfirmationPage,
    CableTransactionPage,
    CreatePinPage,
    DataConfirmationPage,
    DataTransationPage,
    ConfirmPinPage,
    LoginWalletPage,
    NetworkPopupPage,
    CableNetworkPage,
    CompanyPopupPage,
      ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
