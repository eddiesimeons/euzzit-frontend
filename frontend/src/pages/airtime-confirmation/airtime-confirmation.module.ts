import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AirtimeConfirmationPage } from './airtime-confirmation';

@NgModule({
  declarations: [
    AirtimeConfirmationPage,
  ],
  imports: [
    IonicPageModule.forChild(AirtimeConfirmationPage),
  ],
})
export class AirtimeConfirmationPageModule {}
