import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AirtimeTransactionPage } from '../airtime-transaction/airtime-transaction';
import { Login } from '../login/login';

/**
 * Generated class for the AirtimeConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-airtime-confirmation',
  templateUrl: 'airtime-confirmation.html',
})
export class AirtimeConfirmationPage {
  user:any={}
  
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeConfirmationPage');
  }
  Login(){
    this.navCtrl.push(AirtimeTransactionPage)
  }
}
