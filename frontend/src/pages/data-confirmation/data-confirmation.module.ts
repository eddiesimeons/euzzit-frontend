import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataConfirmationPage } from './data-confirmation';

@NgModule({
  declarations: [
    DataConfirmationPage,
  ],
  imports: [
    IonicPageModule.forChild(DataConfirmationPage),
  ],
})
export class DataConfirmationPageModule {}
