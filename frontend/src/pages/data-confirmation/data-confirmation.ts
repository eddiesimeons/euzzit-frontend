import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataTransationPage } from '../data-transation/data-transation';

/**
 * Generated class for the DataConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-data-confirmation',
  templateUrl: 'data-confirmation.html',
})
export class DataConfirmationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataConfirmationPage');
  }
  Login(){
    this.navCtrl.push(DataTransationPage)
  }
}
