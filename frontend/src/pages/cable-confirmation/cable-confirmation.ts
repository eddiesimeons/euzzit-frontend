import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CableTransactionPage } from '../cable-transaction/cable-transaction';

/**
 * Generated class for the CableConfirmationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cable-confirmation',
  templateUrl: 'cable-confirmation.html',
})
export class CableConfirmationPage {
  user:any={}
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CableConfirmationPage');
  }
   Login(){
   this.navCtrl.push(CableTransactionPage )
   }
}
