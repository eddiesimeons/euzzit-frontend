import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CableConfirmationPage } from './cable-confirmation';

@NgModule({
  declarations: [
    CableConfirmationPage,
  ],
  imports: [
    IonicPageModule.forChild(CableConfirmationPage),
  ],
})
export class CableConfirmationPageModule {}
