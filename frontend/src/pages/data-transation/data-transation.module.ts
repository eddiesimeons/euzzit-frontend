import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataTransationPage } from './data-transation';

@NgModule({
  declarations: [
    DataTransationPage,
  ],
  imports: [
    IonicPageModule.forChild(DataTransationPage),
  ],
})
export class DataTransationPageModule {}
