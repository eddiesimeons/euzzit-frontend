import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { AirtimeTransactionSucessfullPage } from '../airtime-transaction-sucessfull/airtime-transaction-sucessfull';

/**
 * Generated class for the DataTransationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-data-transation',
  templateUrl: 'data-transation.html',
})
export class DataTransationPage {

 
  pin: any = []

  user: any = {}

  constructor(public navCtrl: NavController, private _navParams: NavParams, public popoverCtrl: PopoverController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataTransationPage');
  }
  Login() {
    this.navCtrl.push(AirtimeTransactionSucessfullPage);
    
  }

  enterInput(ele) {
    if(this.pin.length <= 4){
      this.pin.push(ele);
    }
  }

  clearInput(){
    if(this.pin.length > 0){
      this.pin.pop();
    }
  }
  // this.navCtrl.push( AirtimeTransactionSucessfullPage)


}
