import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AirtimePage } from '../airtime/airtime';
import { DataPage } from '../data/data';

/**
 * Generated class for the AirtimeDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-airtime-data',
  templateUrl: 'airtime-data.html',
})
export class AirtimeDataPage {
  pages = [
    { pageName: AirtimePage, title: 'Airtime', icon: 'flame', id: 'newsTab'},
    { pageName: DataPage, title: 'Data', icon: 'help-circle', id: 'aboutTab'},
    // { pageName: 'AccountPage', title: 'Body', icon: 'body', id: 'accountTab'}
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeDataPage');
  }

}
