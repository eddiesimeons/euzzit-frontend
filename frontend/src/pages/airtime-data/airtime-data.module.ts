import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AirtimeDataPage } from './airtime-data';

@NgModule({
  declarations: [
    AirtimeDataPage,
  ],
  imports: [
    IonicPageModule.forChild(AirtimeDataPage),
  ],
})
export class AirtimeDataPageModule {}
