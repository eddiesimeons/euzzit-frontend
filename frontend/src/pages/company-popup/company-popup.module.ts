import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompanyPopupPage } from './company-popup';

@NgModule({
  declarations: [
    CompanyPopupPage,
  ],
  imports: [
    IonicPageModule.forChild(CompanyPopupPage),
  ],
})
export class CompanyPopupPageModule {}
