import { Component, ViewChild } from '@angular/core';
import { NavController, App, Nav } from 'ionic-angular';
import { AirtimeDataPage } from '../airtime-data/airtime-data';
import { CabletvPage } from '../cabletv/cabletv';
import { AirtimePage } from '../airtime/airtime';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Nav) nav: Nav;
  constructor(public navCtrl: NavController, public app: App) {

  }
  cabelTv() {
    this.navCtrl.push(CabletvPage)
  }
  airtimeData() {
    this.app.getRootNav().push(AirtimeDataPage)
    }
  electricity() {
  }

}
