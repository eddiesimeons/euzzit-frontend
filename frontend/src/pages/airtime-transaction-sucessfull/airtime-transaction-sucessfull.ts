import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the AirtimeTransactionSucessfullPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-airtime-transaction-sucessfull',
  templateUrl: 'airtime-transaction-sucessfull.html',
})
export class AirtimeTransactionSucessfullPage {
    user:any= {}
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeTransactionSucessfullPage');
  }
  Login(){
    this.navCtrl.setRoot(TabsPage)
  }
}
