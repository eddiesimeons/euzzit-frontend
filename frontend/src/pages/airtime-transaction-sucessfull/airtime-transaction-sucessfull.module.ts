import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AirtimeTransactionSucessfullPage } from './airtime-transaction-sucessfull';

@NgModule({
  declarations: [
    AirtimeTransactionSucessfullPage,
  ],
  imports: [
    IonicPageModule.forChild(AirtimeTransactionSucessfullPage),
  ],
})
export class AirtimeTransactionSucessfullPageModule {}
