import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { LoginWalletPage } from '../login-wallet/login-wallet';

/**
 * Generated class for the ConfirmPinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-pin',
  templateUrl: 'confirm-pin.html',
})
export class ConfirmPinPage {


  pin: any = []

  user: any = {}

  constructor(public navCtrl: NavController, private _navParams: NavParams, public popoverCtrl: PopoverController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeTransactionPage');
  }
  Login() {
    this.navCtrl.push(LoginWalletPage)
   }

  enterInput(ele) {
    if(this.pin.length <= 4){
      this.pin.push(ele);
    }
  }

  clearInput(){
    if(this.pin.length > 0){
      this.pin.pop();
    }
  }
  // this.navCtrl.push( AirtimeTransactionSucessfullPage)


}
