import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmPinPage } from './confirm-pin';

@NgModule({
  declarations: [
    ConfirmPinPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmPinPage),
  ],
})
export class ConfirmPinPageModule {}
