import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CableTransactionPage } from './cable-transaction';

@NgModule({
  declarations: [
    CableTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(CableTransactionPage),
  ],
})
export class CableTransactionPageModule {}
