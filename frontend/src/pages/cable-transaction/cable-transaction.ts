import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { AirtimeTransactionSucessfullPage } from '../airtime-transaction-sucessfull/airtime-transaction-sucessfull';

/**
 * Generated class for the CableTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cable-transaction',
  templateUrl: 'cable-transaction.html',
})
export class CableTransactionPage {

  constructor(public navCtrl: NavController, private _navParams: NavParams,public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeTransactionPage');
  }
  Login(){
     let popover = this.popoverCtrl.create(AirtimeTransactionSucessfullPage);
      popover.present()
    }

}
