import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AirtimeTransactionPage } from './airtime-transaction';

@NgModule({
  declarations: [
    AirtimeTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(AirtimeTransactionPage),
  ],
})
export class AirtimeTransactionPageModule {}
