import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CabletvPage } from './cabletv';

@NgModule({
  declarations: [
    CabletvPage,
  ],
  imports: [
    IonicPageModule.forChild(CabletvPage),
  ],
})
export class CabletvPageModule {}
