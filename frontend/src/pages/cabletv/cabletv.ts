import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { AirtimePage } from '../airtime/airtime';
import { CableConfirmationPage } from '../cable-confirmation/cable-confirmation';
import { AirtimeConfirmationPage } from '../airtime-confirmation/airtime-confirmation';
import { CableNetworkPage } from '../cable-network/cable-network';

/**
 * Generated class for the CabletvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cabletv',
  templateUrl: 'cabletv.html',
})
export class CabletvPage {
  user: any ={}
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimePage');
  }
  Login(){
    let popover = this.popoverCtrl.create(AirtimeConfirmationPage);
    popover.present()
  }

  openPopup() {
    let popover = this.popoverCtrl.create(CableNetworkPage);
    popover.present()
  }
}
