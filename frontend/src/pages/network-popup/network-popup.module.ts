import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NetworkPopupPage } from './network-popup';

@NgModule({
  declarations: [
    NetworkPopupPage,
  ],
  imports: [
    IonicPageModule.forChild(NetworkPopupPage),
  ],
})
export class NetworkPopupPageModule {}
