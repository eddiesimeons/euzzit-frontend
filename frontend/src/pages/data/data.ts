import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { DataConfirmationPage } from '../data-confirmation/data-confirmation';

/**
 * Generated class for the DataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-data',
  templateUrl: 'data.html',
})
export class DataPage {

  user: any ={}
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimePage');
  }
  Login(){
    let popover = this.popoverCtrl.create(DataConfirmationPage);
    popover.present()
  }
  
    // this.navCtrl.push(AirtimeConfirmationPage)
  
}

