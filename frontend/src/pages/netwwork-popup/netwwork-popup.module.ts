import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NetwworkPopupPage } from './netwwork-popup';

@NgModule({
  declarations: [
    NetwworkPopupPage,
  ],
  imports: [
    IonicPageModule.forChild(NetwworkPopupPage),
  ],
})
export class NetwworkPopupPageModule {}
