import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { AccountPage } from '../account/account';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
user: any ={}
 userDetail: any;
constructor(public navCtrl: NavController, public navParams: NavParams) {
     this.userDetail=[
   
   {
    name:     'Vinay',
     age:     23,
     address: 'BSR'
     
  },
   {
     name: 'Laxmiraj',
     age: 23,
     address:'noida'
   
   },
    {
      name: 'sandeep',
     age: 23,
     address:'mathura'
     
   },
   {
     name: 'rohit',
     age: 23,
     address:'dadri'
     
  },
   {
     name: 'deepak',
     age: 23,
     address:'aligarh'
     
  },  ]
}
}