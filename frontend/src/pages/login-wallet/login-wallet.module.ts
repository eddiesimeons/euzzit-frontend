import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginWalletPage } from './login-wallet';

@NgModule({
  declarations: [
    LoginWalletPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginWalletPage),
  ],
})
export class LoginWalletPageModule {}
