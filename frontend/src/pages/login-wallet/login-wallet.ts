import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Register1Page } from '../register1/register1';

/**
 * Generated class for the LoginWalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-wallet',
  templateUrl: 'login-wallet.html',
})
export class LoginWalletPage {
  user: any ={}
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginWalletPage');
  }
  submit() {
    this.navCtrl.push(Register1Page)
}
  Login() {
    this.navCtrl.setRoot(TabsPage)
}
}
