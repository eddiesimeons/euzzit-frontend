import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ConfirmPinPage } from '../confirm-pin/confirm-pin';
// import { HomePage } from '../home/home';

/**
 * Generated class for the CreatePinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-pin',
  templateUrl: 'create-pin.html',
})
export class CreatePinPage {
  pin: any = []
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatePinPage');
  }
  Login() {
   this.navCtrl.push(ConfirmPinPage)
  }

  enterInput(ele) {
    if(this.pin.length <= 4){
      this.pin.push(ele);
    }
  }

  clearInput(){
    if(this.pin.length > 0){
      this.pin.pop();
    }
  }
}
