import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CableNetworkPage } from './cable-network';

@NgModule({
  declarations: [
    CableNetworkPage,
  ],
  imports: [
    IonicPageModule.forChild(CableNetworkPage),
  ],
})
export class CableNetworkPageModule {}
