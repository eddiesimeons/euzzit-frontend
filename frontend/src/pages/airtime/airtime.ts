import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { AirtimeConfirmationPage } from '../airtime-confirmation/airtime-confirmation';
import { NetworkPopupPage } from '../network-popup/network-popup';

/**
 * Generated class for the AirtimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-airtime',
  templateUrl: 'airtime.html',
})
export class AirtimePage {
user: any ={}
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimePage');
  }
  Login(){
    let popover = this.popoverCtrl.create(AirtimeConfirmationPage);
    popover.present()
  }
  
  openNetwork() {
    let popover = this.popoverCtrl.create(NetworkPopupPage);
    popover.present()
  }
    // this.navCtrl.push(AirtimeConfirmationPage)
  
}
